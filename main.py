import json
import traceback
from pandas import DataFrame

from utils import *

try:
    # Get list url from file txt
    urls = []
    with open('data-urls/'+ get_date() +'.txt', 'r') as f:
        for line in f:
            url = line[line.find('http'):].strip()
            if len(url) > 0:
                urls.append(url)

    data_title, data_describe, data_content, count_images = [], [], [], []
    err_msg = 'ERROR: '

    folder = 'data-images/' + get_date()
    k = 1
    while os.path.exists(folder):
        folder = 'data-images/' + get_date() +'_'+ fill_zero(k)
        k = k + 1
    os.makedirs(folder)

    for i, url in enumerate(urls):
        print('- Link #'+ str(i + 1) +': '+ url)
        try:
            obj = create_object_from_url(url)

            # get title
            try:
                data_title.append(obj.get_title())
            except Exception as e:
                data_title.append(err_msg + url)
                print('# '+ err_msg +'Get title - '+ str(e))

            # get describe
            try:
                data_describe.append(obj.get_describe())
            except Exception as e:
                data_describe.append(err_msg + url)
                print('# '+ err_msg +'Get describe - '+ str(e))

            # get images and content
            try:
                images, content = obj.get_images_and_content()
                data_content.append(content)
                try:
                    download_image(i + 1, images, url, folder)
                    count_images.append(len(images))
                except Exception as e:
                    count_images.append(err_msg + url)
                    print('# '+ err_msg +'Download images - '+ str(e))
            except Exception as e:
                data_content.append(err_msg + url)
                count_images.append(err_msg + url)
                print('# '+ err_msg +'Get content - '+ str(e))
        except Exception as e:
            print('# ERROR Link #'+ str(i + 1))
            print('# '+ err_msg + str(e))
            traceback.print_exc()
            data_title.append(err_msg + url)
            data_describe.append(err_msg + url)
            data_content.append(url)
            count_images.append(0)

    # Save to excel
    data = {
        '#': range(1, len(urls) + 1),
        '1 - Title': data_title,
        '2 - Describe': data_describe,
        '3 - Content': data_content,
        '4 - Count Images': count_images
    }
    df = DataFrame(data)
    folder = 'data-excel'
    if not os.path.exists(folder):
        os.makedirs(folder)
    path_file = os.path.join(os.getcwd(), folder, get_date() +'.xlsx')
    i = 1
    while os.path.exists(path_file):
        path_file = os.path.join(os.getcwd(), folder, get_date() +'_'+ fill_zero(i) +'.xlsx')
        i = i + 1
        # os.remove(path_file)
    if i > 1:
        print('+ Re-new file data on day: '+ str(get_date()))
    df.to_excel(path_file, sheet_name='sheet1', index=False)
    print('+ Saved data to excel file: '+ path_file)
except Exception as exc:
    traceback.print_exc()