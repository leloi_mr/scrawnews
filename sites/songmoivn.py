from bs4 import BeautifulSoup
from sites.isite import isite
from utils import *

class songmoivnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('.content .name-article')[0].text.strip()

    def get_describe(self):
        soup = self.page_html\
            .find('div', {'class': 'content'})\
            .find('h3', {'class': 'title-article'})
        res = soup.text.strip()
        return res

    def get_images_and_content(self):
        soup = self.page_html.find('div', {'class': 'post-detail-content'})
        images = get_list_images(soup)
        remove_tags(soup)
        replace_tag_img(soup)
        res = get_raw_text(soup)
        return images, res