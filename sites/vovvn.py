from bs4 import BeautifulSoup

from sites.isite import isite
from utils import *

class vovvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('.article-info .title.cms-title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html\
            .find('div', {'class': 'article-info'})\
            .find('h3', {'class': 'summary cms-desc'})
        res = replace_list_string(['VOV.VN'], '', soup.text).strip()
        return res

    def get_images_and_content(self):
        soup = BeautifulSoup('', 'html.parser')
        soup.append(self.page_html.find('div', {'id': 'article-body'}))

        images = get_list_images(soup)
        remove_tags(soup)
        strip_images(soup)
        res = get_raw_text(soup)
        return images, res