from bs4 import BeautifulSoup

from sites.isite import isite
from utils import *

class giadinhnetvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('.title-detail h1')[0].text.strip()

    def get_describe(self):
        soup = self.page_html\
            .find('div', {'class': 'detail-content2'})\
            .find('h2', {'class': 'detail-sp'})
        res = replace_list_string(['GiadinhNet'], '', soup.text).strip()
        return res

    def get_images_and_content(self):
        soup = BeautifulSoup('', 'html.parser')
        soup.append(self.page_html.find('div', {'class': 'content-new clear'}))

        images = get_list_images(soup)
        remove_tags(soup)
        strip_images(soup)
        res = get_raw_text(soup)
        return images, res