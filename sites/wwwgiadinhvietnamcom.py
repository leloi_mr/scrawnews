from bs4 import BeautifulSoup

from sites.isite import isite
from utils import *

class wwwgiadinhvietnamcomclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('.article .title_hot')[0].text.strip()

    def get_describe(self):
        soup = self.page_html\
            .find('div', {'class': 'article'})\
            .find('div', {'class': 'w100 intro_detail fl'})
        res = soup.text.strip()
        return res

    def get_images_and_content(self):
        article = self.page_html.find('div', {'class': 'main-content article'})
        soup = BeautifulSoup('', 'html.parser')
        soup.append(article.find('div', {'id': 'content_detail'}))

        images = get_list_images(soup)
        remove_tags(soup)
        strip_images(soup)
        res = get_raw_text(soup)
        return images, res