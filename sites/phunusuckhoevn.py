from bs4 import BeautifulSoup

from sites.isite import isite
from utils import *

class phunusuckhoevnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('article .title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html.find('article').find('div', {'class': 'sapo'})
        try:
            soup.a.decompose()
        except Exception as e:
            pass
        return soup.text.strip()

    def get_images_and_content(self):
        article = self.page_html.find('article')
        soup = BeautifulSoup('', 'html.parser')
        soup.append(article.find('div', {'id': 'articleTop'}))
        soup.append(article.find('div', {'id': 'articleCenter'}))
        soup.append(article.find('div', {'id': 'articleBottom'}))

        images = get_list_images(soup)
        remove_tags(soup)
        strip_images(soup)
        res = get_raw_text(soup)
        return images, res