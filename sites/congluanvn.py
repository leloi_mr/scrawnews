from bs4 import BeautifulSoup
from sites.isite import isite
from utils import *

class congluanvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('.post-title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html.select('.post-summary')[0].text.strip()
        soup = replace_list_string(['(CLO)'], '', soup)
        return soup

    def get_images_and_content(self):
        soup = self.page_html.find('div', {'class': 'post-content'})
        images = get_list_images(soup)
        remove_tags(soup)
        replace_tag_img(soup)
        res = get_raw_text(soup)
        return images, res