from bs4 import BeautifulSoup
from sites.isite import isite
from utils import *

class vietnamnetvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('h1.title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html.select('.ArticleContent p')[0].text.strip()
        return soup

    def get_images_and_content(self):
        soup = self.page_html.find('div', {'class': 'ArticleContent'})
        begin = soup.find('h2')
        end = soup.find('div', {'class': 'inner-article'})
        remove_from_to(begin, end)
        begin = soup.find('div', {'class': 'clearfix'})
        end = None
        remove_from_to(begin, end)
        images = get_list_images(soup)
        remove_tags(soup)
        replace_tag_img(soup)
        res = get_raw_text(soup)
        return images, res
