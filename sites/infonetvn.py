from bs4 import BeautifulSoup
from sites.isite import isite
from utils import *

class infonetvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('.container .cms-title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html.select('.container .sapo')[0].text.strip()
        return soup

    def get_images_and_content(self):
        soup = self.page_html.find('div', {'id': 'main_detail'})
        images = get_list_images(soup)
        remove_tags(soup)
        replace_tag_img(soup)
        res = get_raw_text(soup)
        return images, res