from bs4 import BeautifulSoup
from sites.isite import isite
from utils import *

class doanhnghiepvnvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('.main-article .detail-title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html\
            .find('div', {'class': 'main-article'})\
            .find('h2', {'class': 'intronews-detail'})
        res = soup.text.strip()
        return res

    def get_images_and_content(self):
        soup = self.page_html.find('div', {'class': 'detail-news'})
        soup.find('div', {'id': 'add'}).extract()
        soup.find('div', {'class': 'spshare'}).extract()
        images = get_list_images(soup)
        remove_tags(soup)
        replace_tag_img(soup)
        res = get_raw_text(soup)
        return images, res