from bs4 import BeautifulSoup

from sites.isite import isite
from utils import *

class wwwnguoiduatinvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('.contents .title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html\
            .find('div', {'class': 'article-content'})\
            .find('h2', {'class': 'f-rsb'})
        res = soup.text.strip()
        return res

    def get_images_and_content(self):
        soup = BeautifulSoup('', 'html.parser')
        soup.append(self.page_html.find('div', {'class': 'article-content'}))

        images = get_list_images(soup)
        remove_tags(soup)
        strip_images(soup)
        res = get_raw_text(soup)
        return images, res