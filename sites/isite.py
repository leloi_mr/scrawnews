import requests
from bs4 import BeautifulSoup

class isite():
    def get_page_html(self):
        try:
            self.result = requests.get(self.url, timeout=30)
            if self.result.status_code != 200:
                raise Exception("Url errors: "+ self.url)
            return BeautifulSoup(self.result.content.decode('utf-8'), "html.parser")
        except Exception as e:
            print("# ERROR - " + str(e))

