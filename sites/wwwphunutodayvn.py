from bs4 import BeautifulSoup
from sites.isite import isite
from utils import *

class wwwphunutodayvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('article .title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html.find('article')\
            .find('div', {'class': 'intro'})
        soup.span.extract()
        return soup.text.strip()

    def get_images_and_content(self):
        soup = self.page_html.find('div', {'class': 'content_detail_text'})
        images = get_list_images(soup)
        remove_tags(soup)
        replace_tag_img(soup)
        res = get_raw_text(soup)
        return images, res