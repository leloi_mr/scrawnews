from bs4 import BeautifulSoup
from sites.isite import isite
from utils import *

class wwwphunuvagiadinhvnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('article .title')[0].text.strip()

    def get_describe(self):
        return self.page_html.select('article .sapo')[0].text.strip()

    def get_images_and_content(self):
        soup = self.page_html.find('div', {'id': 'article-content'})
        remove_from_to(soup.find('div', {'class': 'bottom-posts'}), None)
        images = get_list_images(soup)
        remove_tags(soup)
        replace_tag_img(soup)
        res = get_raw_text(soup)
        return images, res