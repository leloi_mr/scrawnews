# encoding=utf8
import sys
reload(sys)
sys.setdefaultencoding('utf8')
from bs4 import BeautifulSoup

from sites.isite import isite
from utils import *

class giaoducthoidaivnclass(isite):
    def __init__(self, url):
        self.url = url
        self.page_html = self.get_page_html()

    def get_title(self):
        return self.page_html.select('#articlecontent .cms-title')[0].text.strip()

    def get_describe(self):
        soup = self.page_html\
            .find('div', {'class': 'summary cms-desc'})
        res = replace_list_string(['GD&TĐ'], '', soup.text).strip()
        return res

    def get_images_and_content(self):
        soup = BeautifulSoup('', 'html.parser')
        soup.append(self.page_html.find('div', {'id': 'abody'}))

        images = get_list_images(soup)
        remove_tags(soup)
        strip_images(soup)
        res = get_raw_text(soup)
        return images, res