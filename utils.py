import re
import importlib
import os
import traceback
import datetime
import urllib2

from urlparse import urlparse
from bs4 import Comment, BeautifulSoup

def fill_zero(txt):
    return '0'*(3 - len(str(txt))) + str(txt)

def replace_list_string(list, new_word, txt):
    big_regex = re.compile('|'.join(map(re.escape, list)))
    res = big_regex.sub(new_word, txt)
    return res

def create_object_from_url(url):
    class_name = convert_url_to_class_name(url)
    module_name = 'sites.'+ class_name
    class_name = class_name +'class'
    obj = convert_string_to_class(module_name, class_name)(url)
    return obj

def convert_url_to_class_name(url):
    prohibitedWords = ['.', '-']
    replacedWord = ''
    res = replace_list_string(prohibitedWords, replacedWord, urlparse(url).netloc)
    if str(res[0]).isdigit():
        res = 'www'+ res
    return res

def convert_string_to_class(module_name, class_name):
    # load the module, will raise ImportError if module cannot be loaded
    m = importlib.import_module(module_name)
    # get the class, will raise AttributeError if class cannot be found
    c = getattr(m, class_name)
    return c

def get_list_images(soup):
    res = []
    for img in soup.find_all('img'):
        url = str(img.get('src').encode('utf-8'))
        if not url.startswith('data:') and url.lower().endswith(('.jpg', '.png', '.gif', '.tiff', '.bmp')):
            res.append(url)
    return res

def remove_tags(soup):
    for element in soup(text=lambda text: isinstance(text, Comment)):
        element.extract()
    tags = ['style', 'script', 'adrender']
    for tag in soup.find_all(tags):
        tag.extract()

def strip_images(soup):
    if 'figure' in str(soup):
        replace_tag_figure(soup)
    else:
        replace_tag_img(soup)

def replace_tag_img(soup):
    for i, tag in enumerate(soup.find_all('img')):
        tag.replace_with('\n######## IMAGE %s ########\n' % (i + 1))

def replace_tag_figure(soup):
    for i, tag in enumerate(soup.find_all(['figure'])):
        tag.replace_with('\n######## IMAGE '+ str(i + 1) +' ########\n')

def remove_from_to(begin, end):
    try:
        while begin.next_sibling != end:
            begin.next_sibling.extract()
        begin.extract()
        if end != None:
            end.extract()
    except Exception as e:
        pass

def get_raw_text(soup):
    replace_tag_inline(soup)
    replace_tag_block(soup)
    res = strip_new_line(soup)
    return BeautifulSoup(res, "html.parser").text

def replace_tag_block(soup):
    list = ['h6', 'h5', 'h4', 'h3', 'h2', 'h1', 'p', 'address', 'article', 'aside', 'blockquote', 'canvas', 'dd', 'div', 'dl', 'dt', 'fieldset', 'figcaption', 'figure', 'footer', 'form', 'header', 'hr', 'li', 'main', 'nav', 'noscript', 'ol', 'output', 'pre', 'section', 'table', 'tfoot', 'ul', 'video']
    for tag in list:
        for result in soup.find_all(tag):
            result.replace_with('\n'+ result.text +'\n')

def replace_tag_inline(soup):
    list = ['a', 'abbr', 'acronym', 'b', 'bdo', 'big', 'br', 'button', 'cite', 'code', 'dfn', 'em', 'i', 'img', 'input', 'kbd', 'label', 'map', 'object', 'q', 'samp', 'script', 'select', 'small', 'span', 'strong', 'sub', 'sup', 'textarea', 'time', 'tt', 'var']
    for tag in soup.find_all(list):
        tag.replace_with(tag.text)

def strip_new_line(txt):
    txt = str(txt)
    while '\n\n' in txt:
        txt = txt.replace('\n\n', '\n')
    txt = txt.lstrip('\n')
    txt = txt.rstrip('\n')
    return txt.strip()

def get_date():
    return datetime.datetime.now().strftime("%Y-%m-%d")

def download_image(i, list, url, folder):
    try:
        for j, url_image in enumerate(list):
            print(url_image)
            if urlparse(url_image).netloc == '':
                if not str(url_image).startswith('data:'):
                    url_image = urlparse(url).scheme + '://' + urlparse(url).netloc + url_image
            j = fill_zero(j + 1)
            pattern = url_image.split('.')[-1]
            image_name = fill_zero(i) + j +'.'+ pattern
            file_path = os.path.join(os.getcwd(), folder, image_name)
            if not os.path.exists(file_path):
                # urllib.urlretrieve(url_image, file_path)
                try:
                    request = urllib2.urlopen(url_image, timeout=30)
                    with open(file_path, 'wb') as f:
                        try:
                            f.write(request.read())
                        except Exception as e:
                            print("# ERROR - Save image - "+ str(e))
                    print('+ Downloaded image: %s' % file_path)
                except Exception as e:
                    print(url_image)
                    traceback.print_exc()
    except Exception as e:
        traceback.print_exc()